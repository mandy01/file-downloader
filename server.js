var express = require('express');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var app = express();
var path=require('path');
var pg=require('pg');
app.use(cookieParser());
app.set('view engine', 'html');
app.engine('html', require('ejs').renderFile);
app.use(express.static(path.resolve(__dirname,'static_files')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));


var pg = require('pg');
var path=require('path');
var connectionString = 'pg://mandeep:123456@localhost:5433/mandeep'||process.env.DATABASE_URL ;
var client = new pg.Client(connectionString);
client.connect();

var list_arr=[];
app.post('/sendConfigId',function(req,res){
	console.log("Here is the config Id "+req.body.id);
	var query = client.query("SELECT unnest(links) as links FROM documents WHERE config_id="+req.body.id);

	query.on("row", function (row, result) {
		list_arr.push(row);
	    result.addRow(row);
	});

	query.on("end", function (result) {
		console.log(list_arr);
	    client.end();
	});
	return list_arr;
});


app.get('/', function(req, res) {

	    res.render(path.resolve(__dirname,'views','pages','index.html'));
});

app.get('/downloadLinks', function(req, res) {

	    res.render(path.resolve(__dirname,'views','pages','download.html'),{idata:JSON.stringify(list_arr)});
});

app.listen(8082);
console.log('8082 is the magic port');